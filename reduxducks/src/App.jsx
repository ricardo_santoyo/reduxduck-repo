import React from "react";
import { auth } from "./firebase";
import Pokemons from "./components/Pokemons";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import Login from "./components/Login";
import Container from "@material-ui/core/Container";
import Navbar from "./components/Navigation/Navbar";

import { ThemeProvider } from "@material-ui/styles";
import theme from "./themeConfig";
import Typography from "@material-ui/core/Typography";
import Profile from "./components/Profile";
import MyBurgers from "./components/MyBurgers";
import CreateBurger from "./components/CreateBurger";


import Categories from './components/Admin/Categories';
import Items from './components/Admin/Items/Items';


const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            marginTop: theme.spacing(8),
            //backgroundColor: "red" 
        },
    })
);

function App() {
    const classes = useStyles();
    const [firebaseUser, setFirebaseUser] = React.useState(false);
    const fetchUser = () => {
        auth.onAuthStateChanged((user) => {
            if (user) {
                setFirebaseUser(user);
            } else {
                setFirebaseUser(null);
            }
        });
    };
    React.useEffect(() => {
        fetchUser();
    }, []);

    const PrivateRoute = ({ component, path, ...rest }) => {
        if (localStorage.getItem("user")) {
            const userStorage = JSON.parse(localStorage.getItem("user"));
            if (userStorage.uid === firebaseUser.uid) {
                return <Route component={component} path={[path]} {...rest} />;
            } else {
                return <Redirect to="/login" {...rest} />;
            }
        } else {
            return <Redirect to="/login" {...rest} />;
        }
    };

    return firebaseUser !== false ? (
        <ThemeProvider theme={theme}>
            <Router>
                <Navbar />
                {/* //ADD TO INCLUDE A NEW ROUTE */}
                <Container className={classes.root}>
                    <Switch>
                        <PrivateRoute
                            component={MyBurgers}
                            path="/"
                            exact
                        />
                        <PrivateRoute
                            component={CreateBurger}
                            path="/createburger/:burgerid"
                            
                        />
                         <PrivateRoute
                            component={CreateBurger}
                            path="/createburger"
                            exact
                        />
                        {/* <PrivateRoute
                            component={MyBurgers}
                            path="/myburgers"
                            exact
                        /> */}

                        <PrivateRoute
                            component={Pokemons}
                            path="/pokemons"
                            exact
                        />
                        <PrivateRoute
                            component={Profile}
                            path="/profile"
                            exact
                        />
                        <PrivateRoute
                            component={Categories}
                            path="/categories"
                            exact
                        />
                        <PrivateRoute
                            component={Items}
                            path="/items"
                            exact
                        />
                        <Route component={Login} path="/login" exact />
                    </Switch>
                </Container>
            </Router>
        </ThemeProvider>
    ) : (
        <Typography variant="h6" color="initial">
            Loading
        </Typography>
    );
}

export default App;
