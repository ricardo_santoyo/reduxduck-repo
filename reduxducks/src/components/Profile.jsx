import React from "react";
import Container from "@material-ui/core/Container";
import {
    makeStyles,
    Card,
    CardActionArea,
    CardMedia,
    CardContent,
    Typography,
    CardActions,
    Button,
    TextField,
    Box,
    LinearProgress,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {  updateImageAction } from "../redux/userDucks";
import { updateUserAction } from "./../redux/userDucks";
import { Alert } from "@material-ui/lab/";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    media: {
        height: 345,
    },
    input: {
        display: "none",
    },
}));

const Profile = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { user } = useSelector((store) => store.user);
    const { loading } = useSelector((store) => store.user);
    const [userName, setUserName] = React.useState(user.displayName);
    const [activeForm, setActiveForm] = React.useState(false);
    const [error, setError] = React.useState(false);
    const updateUser = () => {
        if (!userName.trim()) {
            console.log("empty username");
            return;
        }
        setActiveForm(false);
        dispatch(updateUserAction(userName));
    };
    const selectFile = (e) => {
        const clientImage = e.target.files[0];
        console.log(clientImage);
        if (clientImage === undefined) {
            console.log("no image selected");
            return;
        }
        if (
            clientImage.type === "image/png" ||
            clientImage.type === "image/jpeg"
        ) {
            dispatch(updateImageAction(clientImage));
            setError(false);
        } else {
            setError(true);
        }
    };

    return (
        <Container maxWidth="xs" className={classes.root}>
            <Card>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={user.photoURL}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        {loading && <LinearProgress />}
                        <Typography
                            gutterBottom
                            variant="subtitle1"
                            component="h2">
                            User: {user.displayName}
                        </Typography>
                        <Typography
                            gutterBottom
                            variant="subtitle1"
                            component="h2">
                            Email: {user.email}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button
                        size="small"
                        color="primary"
                        onClick={() => setActiveForm(true)}>
                        edit name
                    </Button>
                    <input
                            accept="image/*"
                            className={classes.input}
                            id="contained-button-file"                            
                            type="file"
                            onChange={(e) => selectFile(e)}
                        />
                        <label htmlFor="contained-button-file">
                            <Button
                               size="small" color="primary"  disabled={loading}
                                component="span"        >
                                Update Image
                            </Button>
                        </label>
                    
                </CardActions>
                {error && (
                    <Alert severity="warning">Only jpg/png are allowed</Alert>
                )}
                {activeForm && (
                    <CardActions>
                        <Box>
                            <TextField
                                id="outlined-multiline-flexible"
                                label="Edit Name"
                                multiline
                                rowsMax={4}
                                value={userName}
                                onChange={(e) => setUserName(e.target.value)}
                                variant="outlined"
                            />
                            <Button variant="contained" color="default" onClick={()=>updateUser()}>
                                update
                            </Button>
                        </Box>
                    </CardActions>
                )}

            </Card>
        </Container>
    );
};

export default Profile;
