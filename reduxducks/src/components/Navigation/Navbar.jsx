import React from "react";
import theme from "../../themeConfig";
//import { Link, NavLink } from "react-router-dom";
import { AppBar, Toolbar } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Lists from "./ListMenu";

const useStyle = makeStyles({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up("sm")]: {
            display: "none",
        },
        display: "none",
    },

    appBar: {
        flexGrow: 1,
        
    },
    menuList: {
       
    },
    grow: {
        flexGrow: 1,
    },
});

const Navbar = (props) => {
    const classes = useStyle();
    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
                <Lists
                //firebaseUser={props.firebaseUser}
                />
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;
