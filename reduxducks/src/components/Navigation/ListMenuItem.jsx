import React from 'react'
import { ListItem, makeStyles } from '@material-ui/core'
import {  NavLink } from "react-router-dom";
import theme from "../../themeConfig";

const useStyle = makeStyles({
    title: {
        textTransform: "uppercase",
        marginRight: theme.spacing(2),
        display:'flex',
        justifyContent: 'center',
        flex: 0.1
      },
    
  });
const ListMenuItem = (props) => {
    const classes = useStyle();
    return (
        <ListItem
        button
        component={NavLink}
        to={props.to}
        activeClassName="Mui-selected"
        className={classes.title}
        exact
      >
        {props.title}
      </ListItem>
    )
}

export default ListMenuItem
