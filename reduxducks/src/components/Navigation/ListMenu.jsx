import React from "react";

import { withRouter } from "react-router-dom";
import { List, makeStyles, Button } from "@material-ui/core";
import ListMenuItem from "./ListMenuItem";
import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../../redux/userDucks";

const useStyle = makeStyles({
    list: {
        display: "flex",
        flexGrow: 1,
        justifyContent: "flex-end",
    },
    verticalList: {
        justifyContent: "flex-start",
        flexDirection: "column",
    },
});

const Lists = (props) => {
    const classes = useStyle();
    const dispatch = useDispatch();
    const active = useSelector((store) => store.user.active);
    //
    const logout = () => {
        dispatch(logoutUser());
        props.history.push("/login");
    };
    return (
        <List className={classes.list}>
            {/* 
            <ListMenuItem title="pokemons" to="/pokemons" /> */}
            
            {active ? (
                <>
                    <ListMenuItem title="My Burgers" to="/" />
                    {/* <ListMenuItem title="Categories" to="/categories" />
                    <ListMenuItem title="items" to="/items" /> */}
                    <ListMenuItem title="Create Burger" to="/createburger/0" />
                    {/* <ListMenuItem title="My Burgers" to="/myburgers" /> */}
                    <ListMenuItem title="profile" to="/profile" />
                    <Button variant="text" color="default" onClick={() => logout()}>
                        Logout
                    </Button>
                </>
            ) : (
                <ListMenuItem title="login" to="/login" />
            )}
        </List>
    );
};

export default withRouter(Lists);
