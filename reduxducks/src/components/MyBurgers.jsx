import React from "react";
import Grid from "@material-ui/core/Grid";
import { Typography, Card, GridList, GridListTileBar, GridListTile, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getUserBurgers } from "../redux/burgerDucks";
import BurgerCard from "./BurgerCard";
import { getItems } from "../redux/itemduck";
import itemsReducer from "./../redux/itemduck";
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    containerList: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        backgroundColor: theme.palette.background.paper,
    },
    titleContainer: {
        backgroundColor: "red",
        width: "100%",
    },
}));
const MyBurgers = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { userBurgers } = useSelector((store) => store.burgerReducer);
    const { items } = useSelector((store) => store.itemsReducer);

    useEffect(() => {
        dispatch(getItems());
        dispatch(getUserBurgers());
      // props.history.push("/createburger/0")
    }, [dispatch, props.history]);

    return (
        <Grid container spacing={1} direction="column" justify="center" alignItems="center" alignContent="center" wrap="nowrap" className={classes.root}>
            <Typography variant="h4" color="initial">
                MY BURGERS
            </Typography>
            <Grid container spacing={1}>
                {userBurgers.map((burger) => {
                    return (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={burger.id}>
                            <BurgerCard burgerData={burger} items={items} createMode={false} history={props.history} />
                        </Grid>
                    );
                })}
            </Grid>
        </Grid>
    );
};

export default MyBurgers;
