import React from "react";
//import Alert from "@material-ui/lab/Alert";

import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../redux/userDucks";

import { makeStyles, Typography, Box, Divider, Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },

    paper: {
        marginTop: theme.spacing(1),
        textAlign: "center",
        color: theme.palette.text.secondary,
    },
    redBg: {
        backgroundColor: "red",
        //width: "100%",
        display: "flex",
        alignItems: "center",
    },
}));

const Login = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const { active } = useSelector((store) => store.user);
    const { loading } = useSelector((store) => store.user);
    React.useEffect(() => {
        active && props.history.push("/");
    }, [active, props.history]);

    return (
        <div className={classes.root}>
            <Box padding={2}>
                <Typography color="primary" align="center" variant="h6">
                    GOOGLE ACCESS
                </Typography>
            </Box>
            <Divider />
            <Box width="75%" m="auto">
                <Button variant="text" color="default" fullWidth disabled={loading} onClick={() => dispatch(loginUser())}>
                    Access
                </Button>
            </Box>
        </div>
    );
};

export default withRouter(Login);
