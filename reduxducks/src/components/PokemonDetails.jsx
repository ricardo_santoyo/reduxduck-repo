import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import {
    Typography,
    Card,
    CardMedia,
    CardActionArea,
    CardContent,

} from "@material-ui/core";
import { getSinglePokemon } from "../redux/pokeDucks";
import { useDispatch, useSelector } from "react-redux";

const PokemonDetails = () => {
    const dispatch = useDispatch();
    React.useEffect(() => {
        const fetchData = () => {
            dispatch(getSinglePokemon());
        };
        fetchData();
      
       
    }, [dispatch]);
    const useStyles = makeStyles((theme) =>
        createStyles({
            card: {
                margin: theme.spacing(2),
            },
        })
    );
    const classes = useStyles();

    const pokemon = useSelector((store) => store.pokemons.pokemonDetails);
 

    return pokemon ? (

        <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="140"
                    image={pokemon.imgSource}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {[pokemon.name]}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p">
                       Height: {pokemon.height}, Weight: {pokemon.weight}
                    </Typography>
                </CardContent>
            </CardActionArea>
            
        </Card>
    ):(
        <Typography variant="caption" color="initial">no poke details</Typography>
    )
};

export default PokemonDetails;
