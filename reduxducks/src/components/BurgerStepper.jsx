import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {
    TextField,
    Typography,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    ExpansionPanel,
    makeStyles,
    Button,
    LinearProgress,
    Radio,
    FormControl,
    FormControlLabel,
    FormLabel,
    RadioGroup,
    Checkbox,
    FormGroup,
} from "@material-ui/core";
import { saveBurger } from "../redux/burgerDucks";
import Alert from "@material-ui/lab/Alert";
import { getItems } from "../redux/itemduck";
import { updateBurger } from './../redux/burgerDucks';

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: "33.33%",
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    textField: {
        width: "25ch",
    },
}));

const BurgerStepper = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [expanded, setExpanded] = React.useState(false);

    const { burger } = useSelector((store) => store.burgerReducer);
    const { error } = useSelector((store) => store.burgerReducer);
    const { loading } = useSelector((store) => store.burgerReducer);
    const { items } = useSelector((store) => store.itemsReducer);
    const { isEditMode } = useSelector((store) => store.burgerReducer);

    //const [state, setState] = React.useState([]);
    //const { gilad, jason, antoine } = state

    React.useEffect(() => {
        dispatch(getItems());
    }, [dispatch]);

    const handleChangePanel = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    const savetem = (e) => {
        dispatch(saveBurger())
    }
    const handleChangeBurger = (prop) => (event) => {

        dispatch(updateBurger(prop, event.target.value));
    };

    const handleChange = (prop) => (event) => {
        dispatch(updateBurger(prop, event.target.id, event.target.checked));
    };

    const getCreationItemName = (itemId) => {
        return items.filter((i) => i.id === itemId)[0] && items.filter((i) => i.id === itemId)[0].name;
    };

    return (
        <div className={classes.root}>

            <ExpansionPanel expanded={expanded === "namePanel"} onChange={handleChangePanel("namePanel")}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-label="Expand" aria-controls="-content" id="-header">
                    <Typography className={classes.heading}>Name</Typography>
                    <Typography className={classes.secondaryHeading}>{burger.name}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <TextField
                        fullWidth
                        id="outlined-read-only-input"
                        label="Change your burger name"
                        value={burger.name}
                        onChange={handleChangeBurger("name")}
                        variant="outlined"
                    />
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === "BreadPanel"} onChange={handleChangePanel("BreadPanel")}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-label="Expand" aria-controls="-content" id="-header">
                    <Typography className={classes.heading}>Bread</Typography>
                    <Typography className={classes.secondaryHeading}>{getCreationItemName(burger.bread)}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Choose Bread</FormLabel>
                        <RadioGroup row aria-label="position" name="position" defaultValue="top" value={burger.bread} onChange={handleChangeBurger("bread")}>
                            {items
                                .filter((x) => x.category === "Bread")
                                .map((item) => {
                                    return <FormControlLabel key={item.id} value={item.id} control={<Radio />} label={item.name} />;
                                })}
                        </RadioGroup>
                    </FormControl>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === "PattyPanel"} onChange={handleChangePanel("PattyPanel")}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-label="Expand" aria-controls="-content" id="-header">
                    <Typography className={classes.heading}>Patty</Typography>
                    <Typography className={classes.secondaryHeading}>{getCreationItemName(burger.patty)}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Choose Patty</FormLabel>
                        <RadioGroup row aria-label="position" name="position" defaultValue="top" value={burger.patty} onChange={handleChangeBurger("patty")}>
                            {items
                                .filter((x) => x.category === "patty")
                                .map((item) => {
                                    return <FormControlLabel key={item.id} value={item.id} control={<Radio />} label={item.name} />;
                                })}
                        </RadioGroup>
                    </FormControl>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === "CheesePanel"} onChange={handleChangePanel("CheesePanel")}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-label="Expand" aria-controls="-content" id="-header">
                    <Typography className={classes.heading}>Cheese</Typography>
                    <Typography className={classes.secondaryHeading}>{getCreationItemName(burger.cheese)}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Choose Patty</FormLabel>
                        <RadioGroup row aria-label="position" name="position" defaultValue="top" value={burger.cheese} onChange={handleChangeBurger("cheese")}>
                            {items
                                .filter((x) => x.category === "cheese")
                                .map((item) => {
                                    return <FormControlLabel key={item.id} value={item.id} control={<Radio />} label={item.name} />;
                                })}
                        </RadioGroup>
                    </FormControl>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === "ToppingsPanel"} onChange={handleChangePanel("ToppingsPanel")}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-label="Expand" aria-controls="-content" id="-header">
                    <Typography className={classes.heading}>Toppings</Typography>
                    <Typography className={classes.secondaryHeading}>
                        {
                            burger.basicToppings.map(item => {
                                return (getCreationItemName(item) + ' '
                                )
                            })
                        }

                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Choose Toppings</FormLabel>
                        <FormGroup>
                            {
                                
                            items
                                .filter((x) => x.category === "basic-toppings")
                                .map((item) => {
                                    return (
                                        <FormControlLabel key={item.id}
                                            
                                            control={<Checkbox checked={burger.basicToppings.filter(x=>x===item.id).length>0}
                                                onChange={handleChange("basicToppings")} id={item.id} />}
                                            label={item.name}
                                        />
                                    )

                                })}

                        </FormGroup>

                    </FormControl>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            {error && <Alert severity="error">{error}</Alert>}
            {loading && <LinearProgress />}

            <Button variant="contained" color="default" onClick={(e) => savetem(e)}>
                {
                    isEditMode ? ("UPDATE") : ("save")
                }
            </Button>
        </div>
    );
};

export default BurgerStepper;
