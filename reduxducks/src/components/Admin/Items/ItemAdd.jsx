import React, { useState, useEffect } from "react";
import { Box, Grid, makeStyles, TextField, MenuItem, Button } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";


import { getCategories } from "../../../redux/categoryDuck";
import { getItems, createItem } from "../../../redux/itemduck";


const useStyles = makeStyles((theme) => ({
    box: {
        paddingTop: theme.spacing(1),
    },
}));
const ItemAdd = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const { categories } = useSelector((store) => store.categoriesReducer);

    const [newItem, setNewItem] = useState({
        name: "",
        category: "",
        description: "",
        price: 0,
    });

;
    useEffect(() => {
        dispatch(getCategories());
        dispatch(getItems());
    }, [dispatch]);

    const handleChangeName = (e) => {
        setNewItem({ ...newItem, name: e.target.value });
    };
    const handleChangeCategory = (e) => {
        setNewItem({ ...newItem, category: e.target.value });
    };
    const handleChangeDescription = (e) => {
        setNewItem({ ...newItem, description: e.target.value });
    };
    const handleChangePrice = (e) => {
        setNewItem({ ...newItem, price: e.target.value });
    };

    const save = () => {
        dispatch(createItem(newItem));
    };

    return (
        <Grid item xs={12}>
            <Box className={classes.box}>
                <TextField
                    fullWidth
                    id="outlined-read-only-input"
                    label="New Item Name"
                    value={newItem.name}
                    onChange={(e) => handleChangeName(e)}
                    variant="outlined"
                />
            </Box>

            <Box className={classes.box}>
                <TextField
                    fullWidth
                    select
                    id="outlined-read-only-input"
                    label="Category"
                    value={newItem.category}
                    onChange={(e) => handleChangeCategory(e)}
                    variant="outlined">
                    {categories.map((category) => (
                        <MenuItem key={category.id} value={category.name}>
                            {category.name}
                        </MenuItem>
                    ))}
                </TextField>
            </Box>
            <Box className={classes.box}>
                <TextField
                    fullWidth
                    id="outlined-read-only-input"
                    label="Brief description"
                    value={newItem.description}
                    onChange={(e) => handleChangeDescription(e)}
                    variant="outlined"
                />
            </Box>
            <Box className={classes.box}>
                <TextField
                    id="outlined-read-only-input"
                    fullWidth
                    label="Price"
                    value={newItem.price}
                    onChange={(e) => handleChangePrice(e)}
                    variant="outlined"
                />
            </Box>
            <Box className={classes.box}>
                <Button fullWidth variant="contained" color="default" onClick={() => save()}>
                    ADD ITEM
                </Button>
            </Box>
        </Grid>
    );
};

export default ItemAdd;
