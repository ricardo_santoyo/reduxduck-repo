import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { getItems, removeItemAction } from "../../../redux/itemduck";
import { Button, Table, TableContainer, TableHead, TableCell, TableRow, TableBody } from "@material-ui/core";

const ItemsList = () => {
    const dispatch = useDispatch();
    const { items } = useSelector((store) => store.itemsReducer);

    React.useEffect(() => {
        dispatch(getItems());
    }, [dispatch]);

    const deleteItem = (id) => {
        dispatch(removeItemAction(id));
    };
    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="right">Category</TableCell>
                        <TableCell align="right">Price&nbsp;($)</TableCell>
                        <TableCell align="right"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {items.map((item) => {
                        return (
                            <TableRow key={item.id}>
                                <TableCell component="th" scope="item">
                                    {item.name}
                                </TableCell>
                                <TableCell align="right">{item.category}</TableCell>
                                <TableCell align="right">{item.price}</TableCell>
                                <TableCell align="right">
                                    <Button variant="contained" color="default" onClick={(e) => deleteItem(item.id)}>
                                        del
                                    </Button>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default ItemsList;
