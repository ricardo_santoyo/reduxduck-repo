import React from "react";
import { Grid, Typography } from "@material-ui/core";

import ItemsList from "./ItemsList";
import ItemAdd from './../Items/ItemAdd';



const Items = () => {
    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <Typography variant="subtitle1" color="initial" display="block">
                    Add Item
                </Typography>
            </Grid>
            <ItemAdd />

            <Grid item xs={12}>
                <Typography variant="subtitle1" color="initial">
                    LIST ITEMS
                </Typography>
                <ItemsList />
            </Grid>
        </Grid>
    );
};

export default Items;
