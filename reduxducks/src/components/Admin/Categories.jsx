import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { TextField, Button, Box } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { createCategory, getCategories, deleteCategory } from "../../redux/categoryDuck";


const Categories = () => {
    const dispatch = useDispatch();
    const { categories } = useSelector((store) => store.categoriesReducer);
    const [newCategory, setNewCategory] = useState("");
    const addCategory = () => {
        dispatch(createCategory(newCategory));
    };
    const handleChangeBurger = (e) => {
        setNewCategory(e.target.value);
    };
 
    useEffect(() => {
        dispatch(getCategories());
    }, [dispatch]);
    return (
        <Grid container direction="column" justify="center" alignItems="center" alignContent="stretch" wrap="nowrap">
            <Typography variant="h6" color="initial">
                Categories
            </Typography>
            <Grid container direction="row" justify="center" alignItems="center" alignContent="center" wrap="nowrap">
                <TextField
                    fullWidth
                    id="outlined-read-only-input"
                    label="Add category"
                    // value={burger.name}
                    onChange={(e) => handleChangeBurger(e)}
                    variant="outlined"
                />
                <Button variant="contained" color="default" onClick={() => addCategory()}>
                    ADD
                </Button>
            </Grid>
            <Grid container direction="column" justify="center" alignItems="center" alignContent="center" wrap="nowrap">
                {categories.map((category) => (
                    <Grid key={category.id} container direction="row" justify="space-between" alignItems="flex-start" alignContent="stretch" wrap="nowrap">
                        <Typography variant="caption" color="initial">
                            {category.name}
                        </Typography>
                        <Box>
                            {/* 
                            TODO ADD EDIT
                            <Button variant="contained" color="default">
                                edit
                            </Button> */}
                            {/* 
                            TODO: ETECT IF IT HAS ITEMES ATTACHED */}
                            <Button variant="contained" color="default" onClick={()=>dispatch(deleteCategory(category.id))}>
                                delete 
                            </Button>
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Grid>
    );
};

export default Categories;
