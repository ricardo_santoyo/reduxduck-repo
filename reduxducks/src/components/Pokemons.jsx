import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import {
    getPokemonAction,
    getPrevPokemonAction,
    getNextPokemonAction,
    getSinglePokemon
} from "../redux/pokeDucks";

import {
    ListItem,
    List,
    Grid,
    Button,
    Typography,

} from "@material-ui/core";

import PokemonDetails from './PokemonDetails';

const Pokemons = () => {
    const dispatch = useDispatch();
    const pokemons = useSelector((store) => store.pokemons.results);
    const next = useSelector((store) => store.pokemons.next);
    const previous = useSelector((store) => store.pokemons.previous);


    const useStyles = makeStyles((theme) =>
        createStyles({
            root: {
                flexGrow: 1,
            },
            getPokemonsContainter: {
                //backgroundColor: "red",
                display: "flex",
                flexDirection: "column",
                width: "100%",
                padding: theme.spacing(2),
            },
            getPokemonsContainterItemRow: {
                display: "flex",
                justifyContent: "space-between",
            },
            nextButton: {
                margin: theme.spacing(2),
            },
            card: {
                margin: theme.spacing(2),
               // maxWidth: 345,
            },
        })
    );
    const classes = useStyles();

    return (
        <Grid>
            <Grid container direction="column" alignItems="center">
                <Typography variant="h2" color="initial">
                    POKEMONS APP
                </Typography>
            </Grid>
            <Grid container className={classes.root}>
                <Grid container xs={4} direction="column" alignItems="center">
                    <Grid container spacing={1} wrap="nowrap">
                        {pokemons.length === 0 && (
                            <Button
                                className={classes.nextButton}
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={() => dispatch(getPokemonAction())}>
                                Get Pokemons
                            </Button>
                        )}
                        {previous && (
                            <Button
                                className={classes.nextButton}
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={() =>
                                    dispatch(getPrevPokemonAction())
                                }>
                                Prev
                            </Button>
                        )}
                        {next && (
                            <Button
                                className={classes.nextButton}
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={() =>
                                    dispatch(getNextPokemonAction())
                                }>
                                More
                            </Button>
                        )}
                    </Grid>

                    <List
                        direction="column"
                        className={classes.getPokemonsContainter}>
                        {pokemons.map((pokemon) => (
                            <ListItem key={pokemon.name}>
                                <Grid
                                    container
                                    className={
                                        classes.getPokemonsContainterItemRow
                                    }>
                                    <Typography variant="h6" color="initial">
                                        {pokemon.name}
                                    </Typography>
                                    <Button variant="contained" color="default" onClick={() => dispatch(getSinglePokemon(pokemon.url))}>
                                        details
                                    </Button>
                                </Grid>
                            </ListItem>
                        ))}
                    </List>
                </Grid>
                <Grid container xs={8} direction="column" alignItems="center">
                    <PokemonDetails/>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Pokemons;
