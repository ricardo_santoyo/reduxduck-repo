import React from "react";
import {  useParams } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import BurgerStepper from "./BurgerStepper";
import BurgerCard from "./BurgerCard";
import { useSelector, useDispatch } from "react-redux";
import { getItems } from "../redux/itemduck";
import { getBurgerById } from './../redux/burgerDucks';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    titleContainer: {
        // backgroundColor: "red",
        width: "100%",
    },
}));
const CreateBurger = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
   // const { user } = useSelector((store) => store.user);
    const { burger } = useSelector((store) => store.burgerReducer);
    const { isEditMode } = useSelector((store) => store.burgerReducer);
    const { items } = useSelector((store) => store.itemsReducer);    
    const {burgerid} = useParams();
    const { saveCompleted } = useSelector((store) => store.burgerReducer);
    React.useEffect(() => {   
        
        dispatch(getItems());       
        const getBurgerData = () => {
            dispatch(getBurgerById(burgerid))
        };   
        burgerid && getBurgerData();
        saveCompleted && props.history.push("/");
            
        
    }, [saveCompleted, props.history, dispatch, burgerid]);

    return (
        
        <Grid container spacing={1} direction="column" justify="center" alignItems="center" alignContent="center" wrap="nowrap" className={classes.root}>
            <Typography variant="h4" color="initial">
                {
                    isEditMode ?  ("MODIFY") : ("CREATE") 
                }
                
            </Typography>

            <Grid container className={classes.titleContainer} spacing={1} direction="row" justify="flex-start" alignContent="stretch" wrap="nowrap">
                <BurgerStepper />
                <BurgerCard burgerData={burger} items={items} createMode={true}/>
            </Grid>
        </Grid>
    );
};

export default CreateBurger;
