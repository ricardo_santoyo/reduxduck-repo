import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useDispatch, useSelector } from "react-redux";
import { deleteBurger, saveBurger } from "../redux/burgerDucks";
import { SocialMediaIconsReact } from 'social-media-icons-react';





const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        //height: 440
    },
    media: {
        height: 40,
    },
});
const BurgerCard = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { isEditMode } = useSelector((store) => store.burgerReducer);

    const [burger, setBurger] = React.useState({
        id: "",
        userId: "",
        name: "",
        bread: "",
        patty: "",
        basicToppings: []
    });
    React.useEffect(() => {
        props.burgerData && setBurger(props.burgerData);
    }, [props]);

    const getItemName = (itemId) => {
        const filter = props.items.filter((i) => i.id === itemId)[0] && props.items.filter((i) => i.id === itemId)[0].name;
        return filter;
    };
    const getItemsNames = (items) => {
        console.log("sss " + items)
    };

    const editBurger = (burgerID) => {
        props.history.push("/createburger/" + burgerID)
    }

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia className={classes.media} image="/static/images/cards/contemplative-reptile.jpg" title="Contemplative Reptile" />
                <CardContent>
                    <Typography gutterBottom variant="caption" component="h2">
                        Name: {burger.name}
                    </Typography>
                    <Typography gutterBottom variant="caption" component="h2">
                        Bread:
                        {getItemName(burger.bread)}
                    </Typography>
                    <Typography gutterBottom variant="caption" component="h2">
                        patty: {getItemName(burger.patty)}
                    </Typography>
                    <Typography gutterBottom variant="caption" component="h2">
                        cheese: {getItemName(burger.cheese)}
                    </Typography>
                    <Typography gutterBottom variant="caption" component="h2">
                        toppings:{' '}
                        {
                            (burger.basicToppings !== undefined) &&
                            burger.basicToppings.map(item => {
                                return (getItemName(item) + ' '
                                )
                            })
                        }
                    </Typography>


                    <Typography variant="body2" color="textSecondary" component="p">
                        Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                {props.createMode ? (
                    <>
                        <Button size="small" color="primary" onClick={() => dispatch(saveBurger())}>
                            {isEditMode ? "UPDATE" : "SAVE"}
                        </Button>
                    </>
                ) : (
                        <>
                            <Button size="small" color="primary" onClick={() => dispatch(deleteBurger(burger.id))}>
                                Delete
                        </Button>
                            <Button size="small" color="primary" onClick={() => editBurger(burger.id)}>
                                Edit
                        </Button>

                            {/* <Facebook link={'https://crud-brg.web.app/createburger/'+burger.id}/> */}

                            <Button size="small" color="primary" onClick={() => editBurger(burger.id)}>
                                <SocialMediaIconsReact icon="facebook" backgroundColor='gray' url={'https://www.facebook.com/sharer/sharer.php?u=https://crud-brg.web.app/createburger/' + burger.id} />

                            </Button>



                        </>
                    )}
            </CardActions>
        </Card>
    );
};

export default BurgerCard;
