import { db } from "../firebase";
//constantes
const intialData = {
    saveCompleted: false,
    loading: false,
    error: null,
    categories: [],
};

//type
const LOADING = "LOADING";
const ERROR = "ERROR";
const GET_CATEGORIES_SUCCESS = "GET_CATEGORIES_SUCCESS";
const ADD_CATEGORY_SUCCESS = "ADD_CATEGORY_SUCCESS";
const DELETE_CATEGORY_SUCCESS = "DELETE_CATEGORY_SUCCESS";


//reducer
export default function categoriesReducer(state = intialData, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true, error: null };
        case ERROR:
            return { ...state, loading: false, error: action.payload };
        case GET_CATEGORIES_SUCCESS:
            return { ...state, loading: false, error: null, categories: action.payload };
        case ADD_CATEGORY_SUCCESS:
            return { ...state, loading: false, error: null, categories: [...state.categories, action.payload] };
            case DELETE_CATEGORY_SUCCESS:
            return { ...state, loading: false, error: null, categories: action.payload };
        default:
            return state;
    }
}
//action
export const getCategories = () => async (dispatch) => {
    dispatch({ type: LOADING });
    let tempCategories = [];
    try {
        const snapshot = await db.collection("categories").get();
        snapshot.docs.map(function (doc) {
            tempCategories = [...tempCategories, doc.data()];
        });
        dispatch({
            type: GET_CATEGORIES_SUCCESS,
            payload: tempCategories,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error getting categories",
        });
    }
};

export const createCategory = (newCategory) => async (dispatch) => {
    dispatch({ type: LOADING });
    try {
        const ref = db.collection("categories").doc();
        let categoryToSave = {
            name: newCategory,
            id: ref.id,
        };
        await ref.set(categoryToSave);
        dispatch({
            type: ADD_CATEGORY_SUCCESS,
            payload: categoryToSave,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error saving category",
        });
    }
};
export const deleteCategory = (categoryID) => (dispatch, getState) => {
    const { categories } = getState().categoriesReducer;
    try {
        db.collection("categories").doc(categoryID).delete();
        let categoriesToSave = [...categories];
        categoriesToSave = categories.filter((category) => category.id != categoryID);
        dispatch({
            type: DELETE_CATEGORY_SUCCESS,
            payload: categoriesToSave,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error deleting category",
        });
    }
};

