import {  db } from "../firebase";

//constantes
const intialData = {
    saveCompleted: false,
    loading: false,
    error: null,
    items: [],
};

//type
const LOADING = "LOADING";
const ERROR = null;
const ADD_ITEM_SUCCESS = "ADD_ITEM_SUCCESS";
const GET_ITEM_SUCCESS = "GET_ITEM_SUCCESS";
const REMOVE_ITEM_SUCCESS = "REMOVE_ITEM_SUCCESS";

//reducer
export default function itemsReducer(state = intialData, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true, error: null };
        case ERROR:
            return { ...state, loading: false, error: action.payload };
        case ADD_ITEM_SUCCESS:
            return { ...state, loading: false, error: null, items: [...state.items, action.payload] };
        case GET_ITEM_SUCCESS:
            return { ...state, loading: false, error: null, items: action.payload };
        case REMOVE_ITEM_SUCCESS:
            return { ...state, loading: false, error: null, items: action.payload };

        default:
            return state;
    }
}

export const getItems = () => async (dispatch) => {
    dispatch({ type: LOADING });
    try {
        const snapshot = await db.collection("items").get();
        let tempItems = [];
        snapshot.docs.map((item) => {
            tempItems = [...tempItems, item.data()];
        });
        dispatch({
            type: GET_ITEM_SUCCESS,
            payload: tempItems,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error getting items",
        });
    }
};

export const createItem = (item) => async (dispatch) => {
    dispatch({ type: LOADING });
    try {
        const ref = db.collection("items").doc();
        let itemToSave = { ...item, id: ref.id };
        await ref.set(itemToSave);
        dispatch({
            type: ADD_ITEM_SUCCESS,
            payload: itemToSave,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error creating item",
        });
    }
};
export const removeItemAction = (itemId) => (dispatch, getState) => {
    dispatch({ type: LOADING });
    const { items } = getState().itemsReducer;
    try {
        db.collection("items").doc(itemId).delete();
        let tempItems = items.filter((item) => item.id !== itemId);
        dispatch({
            type: REMOVE_ITEM_SUCCESS,
            payload: tempItems,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error removing item categories",
        });
    }
};
