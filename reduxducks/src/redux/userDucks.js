import { auth, firebase, db, storage } from "../firebase";
//constantes

const intialData = {
    loading: false,
    active: false,
};

//type
const LOADING = "LOADING";
const USER_ERROR = "USER_ERROR";
const USER_SUCCESS = "USER_SUCCESS";
const USER_LOGOUT = "USER_LOGOUT";

//reducer
export default function userReducer(state = intialData, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true };
        case USER_ERROR:
            return { ...state, loading: false };
        case USER_LOGOUT:
            return { ...state, loading: false, active: false };
        case USER_SUCCESS:
            return {
                ...state,
                loading: false,
                active: true,
                user: action.payload,
            };
        default:
            return { ...state, loading: false};
    }
}

//action
export const loginUser = () => async (dispatch) => {
    dispatch({
        type: LOADING,
    });
    try {
        const provider = new firebase.auth.GoogleAuthProvider();
        const res = await auth.signInWithPopup(provider);

        const user = {
            uid: res.user.uid,
            email: res.user.email,
            displayName: res.user.displayName,
            photoURL: res.user.photoURL,
        };
        const userDB = await db.collection("users").doc(user.email).get();

        if (userDB.exists) {
            dispatch({
                type: USER_SUCCESS,
                payload: userDB.data(),
            });
            localStorage.setItem("user", JSON.stringify(userDB.data()));
        } else {
            await db.collection("users").doc(user.email).set(user);

            dispatch({
                type: USER_SUCCESS,
                payload: user,
            });
            localStorage.setItem("user", JSON.stringify(user));
        }
    } catch (error) {
        dispatch({
            type: USER_ERROR,
        });
    }
};
export const logoutUser = () => async (dispatch) => {
    localStorage.removeItem("user");
    auth.signOut();
    dispatch({
        type: USER_LOGOUT,
    });
};

export const reactActiveUserAction = () => (dispatch) => {
    if (localStorage.getItem("user")) {
        dispatch({
            type: USER_SUCCESS,
            payload: JSON.parse(localStorage.getItem("user")),
        });
    }
};

export const updateUserAction = (updatedName) => async (dispatch, getState) => {
    dispatch({
        type: LOADING,
    });
    const { user } = getState().user;

    try {
        if (user) {
            user.displayName = updatedName;
            await db.collection("users").doc(user.email).update({
                displayName: updatedName,
            });
            const userUpdate = { ...user, displayName: updatedName };
            localStorage.setItem("user", JSON.stringify(userUpdate));
            dispatch({
                type: USER_SUCCESS,
                payload: userUpdate,
            });
        }
    } catch (error) {
        console.log(error);
    }
};

export const updateImageAction = (editedImage) => async (
    dispatch,
    getState
) => {
    dispatch({
        type: LOADING,
    });
    const { user } = getState().user;
    try {
        const imageRef = await storage
            .ref()
            .child(user.email)
            .child("profile image");
        await imageRef.put(editedImage);
        const photoURL = await imageRef.getDownloadURL();

        await db.collection("users").doc(user.email).update({
            photoURL: photoURL,
        });
        const userUpdate = { ...user, photoURL: photoURL };
        localStorage.setItem("user", JSON.stringify(userUpdate));
        dispatch({
            type: USER_SUCCESS,
            payload: userUpdate,
        });
    } catch (error) {}
};
