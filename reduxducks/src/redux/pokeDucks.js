import Axios from "axios";
//constantes
const intialData = {
    results: [],
    count: 0,
    next: null,
    previous: null,
};

//type
const GET_POKEMON_OK = "GET_POKEMON_OK";
const GET_SINGLE_POKEMON_OK = "GET_SINGLE_POKEMON_OK";
const NEXT_POKEMON_OK = "NEXT_POKEMON_OK";

//reducer
export default function pokeReducer(state = intialData, action) {
    switch (action.type) {
        case GET_POKEMON_OK:
            return { ...state, ...action.payload };
        case NEXT_POKEMON_OK:
            return { ...state, ...action.payload };
        case GET_SINGLE_POKEMON_OK:
            return { ...state, pokemonDetails: action.payload };
        default:
            return state;
    }
}

//action

export const getSinglePokemon = (url = 'https://pokeapi.co/api/v2/pokemon/1/') => async (dispatch, getState) => {
   
    if (localStorage.getItem(url) !=null) {
        dispatch({
            type:  GET_SINGLE_POKEMON_OK,
            payload: JSON.parse(localStorage.getItem(url)),
        });
        return;
    }
    try {
        const res = await Axios.get(url);
    
        dispatch({
            type: GET_SINGLE_POKEMON_OK,
            payload: {
                name: res.data.name,
                weight: res.data.weight,
                height: res.data.height,
                imgSource: res.data.sprites.front_default,
            },
        });
        localStorage.setItem(url, JSON.stringify({
            name: res.data.name,
                weight: res.data.weight,
                height: res.data.height,
                imgSource: res.data.sprites.front_default,
        }));
    } catch (error) {}
  
};

export const getPokemonAction = () => async (dispatch, getState) => {
    if (localStorage.getItem("offset=0")) {
        dispatch({
            type: GET_POKEMON_OK,
            payload: JSON.parse(localStorage.getItem("offset=0")),
        });
        return;
    }
    try {
        const res = await Axios.get(
            `https://pokeapi.co/api/v2/pokemon?offset=0&limit=20`
        );
        dispatch({
            type: GET_POKEMON_OK,
            payload: res.data,
        });
        localStorage.setItem("offset=0", JSON.stringify(res.data));
    } catch (error) {}
};

export const getNextPokemonAction = () => async (dispatch, getState) => {
    const next = getState().pokemons.next;
    if (localStorage.getItem(next)) {
        dispatch({
            type: GET_POKEMON_OK,
            payload: JSON.parse(localStorage.getItem(next)),
        });
        return;
    }

    try {
        const res = await Axios.get(next);
        dispatch({
            type: NEXT_POKEMON_OK,
            payload: res.data,
        });
        localStorage.setItem(next, JSON.stringify(res.data));
    } catch (error) {}
};

export const getPrevPokemonAction = () => async (dispatch, getState) => {
    const previous = getState().pokemons.previous;
    if (localStorage.getItem(previous)) {
        dispatch({
            type: GET_POKEMON_OK,
            payload: JSON.parse(localStorage.getItem(previous)),
        });
        return;
    }
    try {
        const res = await Axios.get(previous);
        dispatch({
            type: NEXT_POKEMON_OK,
            payload: res.data,
        });
        localStorage.setItem(previous, JSON.stringify(res.data));
    } catch (error) {}
};
