import { db } from "../firebase";

//constantes
const intialData = {
    saveCompleted: false,
    loading: false,
    error: null,
    isEditMode: false,
    burger: {
        id: "",
        userId: "",
        name: "",
        bread: "",
        patty: "",
        cheese: "",
        basicToppings: [],
    },
    userBurgers: [],
};

//type
const MODIFIED_BURGER = "MODIFIED_BURGER";
const GET_USER_BURGERS = "GET_USER_BURGERS";
const ERROR = "ERROR";
const SAVE_BURGER_OK = "SAVE_BURGER_OK";
const UPDATE_BURGER_OK = "UPDATE_BURGER_OK";

const LOADING = "LOADING";
const SAVE_BURGER_ERROR = "LOADING_ERROR";
const DELETE_BURGER_SUCCESS = "DELETE_BURGER_SUCCESS";
const GET_BURGER_BY_ID = "GET_BURGER_BY_ID";

//reducer
export default function burgerReducer(state = intialData, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true, error: null, saveCompleted: false };
        case ERROR:
            return { ...state, loading: false, error: action.payload };
        case MODIFIED_BURGER:
            return { ...state, loading: false, error: null, burger: action.payload };
        case SAVE_BURGER_OK:
            return {
                ...state,
                loading: false,
                error: null,
                burger: { id: "", userId: "", name: "", bread: "", patty: "", cheese: "", basicToppings: [] },
                userBurgers: [...state.userBurgers, action.payload],
                saveCompleted: true,
                isEditMode: false,
            };
        case UPDATE_BURGER_OK:
            return {
                ...state,
                loading: false,
                error: null,
                burger: { id: "", userId: "", name: "", bread: "", patty: "", cheese: "", basicToppings: [] },
                userBurgers: action.payload,
                saveCompleted: true,
                isEditMode: false,
            };

        case GET_USER_BURGERS:
            return { ...state, loading: false, error: null, userBurgers: action.payload, isEditMode: false };
        case GET_BURGER_BY_ID:
            return { ...state, loading: false, error: null, burger: action.payload, isEditMode: action.editMode };
        case DELETE_BURGER_SUCCESS:
            return { ...state, loading: false, error: null, userBurgers: action.payload, isEditMode: false };
        case SAVE_BURGER_ERROR:
            return { ...state, loading: false, error: action.payload };

        default:
            return { ...state, loading: false, error: null };
    }
}
//action

export const getUserBurgers = () => async (dispatch, getState) => {
    dispatch({ type: LOADING });
    console.log("getUserBurgers");

    const { user } = getState().user;

    try {
        var snapshot = await db.collection("burgers").where("userId", "==", user.uid).get();
        let userBurgersToUpdate = [];
        snapshot.docs.map(function (doc) {
            userBurgersToUpdate = [...userBurgersToUpdate, doc.data()];
        });
        dispatch({
            type: GET_USER_BURGERS,
            payload: userBurgersToUpdate,
        });
    } catch (error) { }
};

export const getBurgerById = (burgerId) => async (dispatch, getState) => {
    dispatch({ type: LOADING });
    const { user } = getState().user;
    try {
        const burgerQuery = await db.collection("burgers").doc(burgerId).get();

        if (burgerQuery.exists) {
            const isOwnBurger = burgerQuery.data().userId === user.uid;
            dispatch({
                type: GET_BURGER_BY_ID,
                payload: burgerQuery.data(),
                editMode: isOwnBurger,
            });
        } else {
            const emptyBurger = {
                id: "",
                userId: "",
                name: "",
                bread: "",
                patty: "",
                cheese: "",
                basicToppings:[]
            };
            dispatch({
                type: GET_BURGER_BY_ID,
                payload: emptyBurger,
                editMode: false,
            });
        }
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error getting burger",
        });
    }
};

export const updateBurger = (prop, value, checked = false) => (dispatch, getState) => {
    const { burger } = getState().burgerReducer;
    if (prop === "basicToppings") {
        if(checked){
            //todo make it generic
            const burgerToUpdate = { ...burger, [prop]: [...burger.basicToppings, value] }; 

            dispatch({
                type: MODIFIED_BURGER,
                payload: burgerToUpdate,
            });      
        }
        else{
            const top= burger.basicToppings.filter(s=> s!==value);
            const burgerToUpdate = { ...burger, [prop]: top }; 

            dispatch({
                type: MODIFIED_BURGER,
                payload: burgerToUpdate
            });   
        }
    }
    else {
        const burgerToUpdate = { ...burger, [prop]: value };
        dispatch({
            type: MODIFIED_BURGER,
            payload: burgerToUpdate,
        });
    }

};

export const deleteBurger = (burgerId) => async (dispatch, getState) => {
    const { userBurgers } = getState().burgerReducer;
    dispatch({ type: LOADING });
    try {
        await db.collection("burgers").doc(burgerId).delete();
        let tempBurgers = [];
        tempBurgers = userBurgers.filter((burger) => burger.id !== burgerId);
        dispatch({
            type: DELETE_BURGER_SUCCESS,
            payload: tempBurgers,
        });
    } catch (error) {
        dispatch({
            type: ERROR,
            payload: "Error deleteBurger",
        });
    }
};

export const saveBurger = () => async (dispatch, getState) => {
    dispatch({ type: LOADING });
    const { user } = getState().user;
    const { burger } = getState().burgerReducer;
    const { userBurgers } = getState().burgerReducer;
    const { isEditMode } = getState().burgerReducer;

    if (burger.name.trim().length < 3) {
        dispatch({
            type: SAVE_BURGER_ERROR,
            payload: "Not valid name, it needs at least 3 characters",
        });
        return;
    }
    if (burger.bread.trim().length < 3) {
        dispatch({
            type: SAVE_BURGER_ERROR,
            payload: "Please Choose a Bread",
        });
        return;
    }
    if (burger.patty.trim().length < 3) {
        dispatch({
            type: SAVE_BURGER_ERROR,
            payload: "Please Choose a Patty",
        });
        return;
    }
    if (burger.cheese.trim().length < 3) {
        dispatch({
            type: SAVE_BURGER_ERROR,
            payload: "Please Choose Cheese",
        });
        return;
    }
    try {
        let burgerToSave = { ...burger };
        if (isEditMode) {
            await db.collection("burgers").doc(burger.id).update(burger);

            //data: this.state.data.map(el => (el.id === id ? {...el, text} : el))
            //let burgersToUpdate = userBurgers.map(b => (b.id === burgerToSave.id) ? { ...b, burgerToSave } : b);

            const filtered = userBurgers.map(b=> b.id!==burgerToSave.id);
            const burgersToUpdate = [...filtered, burgerToSave]

            dispatch({
                type: SAVE_BURGER_OK,
                payload: burgersToUpdate,
            });
        } else {
            const ref = await db.collection("burgers").doc();
            burgerToSave = { ...burger, userId: user.uid, id: ref.id };
            await ref.set(burgerToSave);
            dispatch({
                type: SAVE_BURGER_OK,
                payload: burgerToSave,
            });
        }
    } catch (error) {
        console.log("e: " + error);
    }
};
