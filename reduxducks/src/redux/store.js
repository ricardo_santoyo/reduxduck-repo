import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import pokeReducer from './pokeDucks';
import userReducer, {reactActiveUserAction} from './userDucks';
import burgerReducer from './burgerDucks';
import categoriesReducer from './categoryDuck';
import itemsReducer from './itemduck'; 



const rootReducer = combineReducers({
    pokemons: pokeReducer,
    user: userReducer, 
    burgerReducer: burgerReducer, 
    categoriesReducer: categoriesReducer, 
    itemsReducer: itemsReducer, 
    
    
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
    reactActiveUserAction()(store.dispatch)
    return store;
}
