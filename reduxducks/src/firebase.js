import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'


const firebaseConfig = {
    apiKey: "AIzaSyCiiv4QXJ-mIHUiHVy0lCiw3jPuh9cLzGc",
    authDomain: "crud-brg.firebaseapp.com",
    databaseURL: "https://crud-brg.firebaseio.com",
    projectId: "crud-brg",
    storageBucket: "crud-brg.appspot.com",
    messagingSenderId: "86753820820",
    appId: "1:86753820820:web:b2065fc9ea99aa6394b481"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const auth = firebase.auth();
  const db = firebase.firestore();
  const storage = firebase.storage();

  export {auth, firebase, db, storage}